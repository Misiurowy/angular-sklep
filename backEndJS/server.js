var express = require('express');
var app = express();
var fs = require("fs");
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var promotions = [];
mongoose.connect('mongodb://localhost/test');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin',"http://localhost:4200"); // * => allow all origins
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS,DELETE,PATCH');
  res.header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Accept,Authorization'); // add remove headers according to your needs
  res.header('Access-Control-Allow-Credentials', 'true');
  next()
})
app.get('/testDelete',function(req,res){
	io.emit('promotion',{productName: 'Football', _id:"5a5bae39a34195015c273620",duration: 0, percentage: 20});
	console.log('sent');
	res.send('done');
});
app.get('/testPromotion',function(req,res){
	io.emit('promotion',{productName: 'Football', _id:"5a5bae39a34195015c273620",duration: 300, percentage: 20});
	setInterval(function(){io.emit('promotion',{productName: 'Football', _id:"5a5bae39a34195015c273620",duration: 0, percentage: 20});},5000);
	console.log('sent');
	res.send('done');
});

app.post('/putPromotion',function(req,res){
	console.log(req.body);
	io.emit('promotion',{_id: req.body._id,duration: req.body.duration, percentage: req.body.percentage});
	var flag = true;
	promotions.forEach(function(prom){
		if(prom._id==req.body._id){
			console.log("repetition");
			flag= false;
		}
	})
	if(flag){
		promotions.push(req.body);
		setTimeout(function(){
		console.log("sending delete");
		io.emit('promotion',{ _id:req.body._id,duration: 0, percentage: 0});
		deletePromotion(req.body._id);
	},req.body.duration*1000);
	console.log('sent');
	res.send('done');
	}else{
		res.send('repetiotion');
	}
	
});
function deletePromotion(id){
	promotions.forEach(function (item){
		if(item._id == id){
			promotions.splice(promotions.indexOf(item),1);
			console.log("deleted prmotion");
		}
	})
		
	
};
function generateSecurityId(){
	return  "aldsfdahdlaskjdfhalsdjfhaslkdjfhsalkjsueyhrjkdcb";
}

app.post('/users/logout',function(req,res){
	var Users = mongoose.model('Users');
	Users.find({securityId: req.securityId},function(err,data){
		if(err)
			console.log(err);
		else{
			if(data[0]!=undefined){
				data[0].securityId = "";
				data[0].loggedIn = false;
				Users.update({_id: data[0]._id},data[0],function(err,data){})
			}
		}
	})
	res.send("logged out");
});

app.post('/users/login',function(req,res){
	var Users = mongoose.model('Users');
	Users.find({username: req.body.username},function(err,data){
		if(err || data[0]== undefined || data[0].password == undefined || data[0].password != req.body.password){
			res.status(400);
			res.send("Uncorrect credentials");
		}else{
			let tmp = generateSecurityId();
			data[0].securityId = tmp;
			data[0].loggedIn = true;
			console.log(data);
			Users.update({_id: data[0]._id},data[0],function(err,data2){
				if(err){ 
					console.log('error');
					console.log(err);
					res.send(err)
				}else{
					setTimeout(function(){invalidate(data[0].securityId)},300000);
					res.json({securityid: data[0].securityId});
				}
			})
			
		};
	})
})
function invalidate(securityId){
	var Users = mongoose.model('Users');
	Users.find({securityId:securityId},function(err,data){
		if(err)
			console.log(err);
		else{
			if(data[0]!=undefined){
			data[0].securityId = "";
			data[0].loggedIn = false;
			Users.update({_id: data[0]._id},data[0],function(err,data){})
		}
		}
	})
}
app.delete('/orders/:id',function(req,res){
	authorize(req.get('Authorization'),function(result){
		console.log(result);
		if(result){
	var Orders = mongoose.model('Orders');
	Orders.remove({_id: req.params.id},function(err,data){
		if(err){ 
			console.log('error');
			console.log(err);
			res.send(err)
		}else{
			console.log(data);
			res.json(data);
		}
	})
	}else{
			res.status(401);
			res.send('400,missing authorization header');
		}
	});

})
app.put('/orders/:id',function(req,res){
		authorize(req.get('Authorization'),function(result){
		console.log(result);
		if(result){
	 		var Orders = mongoose.model('Orders');

	Orders.update({_id: req.params.id},req.body,function(err,data){
		if(err){ 
			console.log('error');
			console.log(err);
			res.send(err)
		}else{
			console.log(data);
			res.json(data);
		}
	})
		}else{
			res.status(401);
			res.send('400,missing authorization header');
		}
	});

})
app.get('/products',function(req,res){
	var Products = mongoose.model('Products');
	Products.find(function(err,data){
		if(err) 
			res.send(err);
		else{
			res.json(data);
			promotions.forEach(function(prom){
				io.emit('promotion',prom);
			})
		}	
			
	})
})
app.get('/orders',function(req,res){
	console.log(req.get('Authorization'));
	authorize(req.get('Authorization'),function(result){
		console.log(result);
		if(result){
	 	var Orders = mongoose.model('Orders');
		Orders.find(function(err,data){
		if(err) 
			res.send(err);
		else
			res.json(data);
		})
		}else{
			res.status(401);
			res.send('400,missing authorization header');
		}
	});

})
app.post('/orders',function(req,res){
	console.log(req.get('Authorization'));
	var Orders = mongoose.model('Orders');
	Orders.create(req.body, function(err,post){
		if(err) 
			res.send(err);
		else
			res.json(post);
	});
})
app.post('/products',function(req,res){
	var Products = mongoose.model('Products');
	Products.create(req.body, function(err,post){
		if(err) 
			res.send(err);
		else
			res.json(post);
	});
})
app.get('/addAdmin',function(req,res){
	var Users = mongoose.model('Users');
	Users.remove(function(err,data){
		if(err){ 
			console.log('error');
			console.log(err);
			res.send(err)
		}else{
			console.log(data);
		}
	});
	Users.create({username: "admin", password: "admin", securityId: "",loggedIn: false}, function(err,post){
		if(err) 
			res.send(err);
		else
			res.json(post);
	});
});
app.get('/getAdmin',function(req,res){
	var Users = mongoose.model('Users');
	Users.find(function(err,data){
		if(err) 
			res.send(err);
		else{
			res.json(data);
		}	
			
	})
});
app.get('/fillproducts',function(req,res){
	var Products = mongoose.model('Products');
	Products.remove(function(err,data){
		if(err){ 
			console.log('error');
			console.log(err);
			res.send(err)
		}else{
			console.log(data);
		}
	})
	Products.create({"name":"Football","description":"A ball? sport? who knows!","price":39,"category":"Football","amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
	Products.create({"name":"Training shoes","description":"Shoes for horse galloping. Really, the name is kinda selfexplanatory","category":"Football","price":99,"amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
	Products.create({"name":"Good shirt","description":"Shirt. Good. Really.","category":"Football","price":48,"amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
	Products.create({"name":"Skis, 2 pack","description":"Get 2, pay as for 1!","category":"Winter sports","price":123,"amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
	Products.create({"name":"Snowboard","description":"Don't get your hopes up, you don't get 2 here.","category":"Winter sports","price":140,"amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
		Products.create({"name":"Skiing poles","description":"Just simple poles. But they're used in skiing so they got fancy name.","category":"Winter sports","price":55,"amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
		Products.create({"name":"Spaghetti","description":"You know it's a sport shop, right?","category":"Pasta","price":5,"amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
	Products.create({"name":"Chessboard","description":"What? It is also a sport...","price":15,"category":"Not so much sports","amount": 10}, function(err, todo){
  		if(err) console.log(err);
  		else console.log(todo);
	});
	Products.find(function(err,data){
		if(err) 
			res.send(err);
		else
			res.json(data);
	})
})
app.get('/mongo',function(req,res){
	var Task = mongoose.model('Task');
	var task = new Task();
	task.project="jakis";
	task.description="cos";
	task.save(function(err) {
		if (err) 
			throw err;
		console.log('Zadanie został o zapisane.');
	});
	Task.find(function(err,data){
		if(err) 
			res.send(err);
		else
			res.json(data);
	})
})
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'błąd połączenia...'));
db.once('open', function() {
	console.log("Db connection up");
});
var Schema = mongoose.Schema;
var Products = new Schema({
	name: String,
	price: Number,
	description: String,
	category: String,
	amount: Number
}) 
mongoose.model('Products',Products);
var Orders = new Schema({
	name: String,
	lastname: String,
	address: String,
	order: String,
	finished: Boolean
})
mongoose.model('Orders',Orders);
var Users = new Schema({
	username: String,
	password: String,
	securityId: String,
	loggedIn: false
})
mongoose.model('Users',Users);
var server = app.listen(5000, function () {
	var host = server.address().address
	var port = server.address().port
console.log("Przykładowa aplikacja nasłuchuje na http://%s:%s", host, port)
});
function authorize(securityId,callback){
	var Users = mongoose.model('Users');
	console.log(securityId);
	Users.find({securityId: securityId},function(err,data){
		console.log(data);
		console.log(err);
		if(err || data[0]== undefined){
			return callback(false);
		}
		if(!data[0].loggedIn){
			return callback(false);
		}
		console.log("callback true");
		return callback(true);
	});
}
var io = require('socket.io' )(server);
io.on('connection' , function(client) {
	console.log('Client connected...' );
	client.emit('messages' , { hello: 'test' });
	client.on('addpromotion',(promotion)=>{
		io.emit('promotion',{name: 'test', price: 1, description: 'test',category: 'test',amount: 1, _id: 123});
	});
});