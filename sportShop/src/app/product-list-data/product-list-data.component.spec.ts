import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListDataComponent } from './product-list-data.component';

describe('ProductListDataComponent', () => {
  let component: ProductListDataComponent;
  let fixture: ComponentFixture<ProductListDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductListDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
