import { Component } from '@angular/core';
import { Product } from '../product/product.component'
@Component({
  selector: 'app-product-list-data',
  templateUrl: './product-list-data.component.html',
  styleUrls: ['./product-list-data.component.css'],
})
export class ProductListData {
	products: Product[] = [];
	total:number = 0;
	categories: string[];

  constructor(products: Product[],total: number, categories: string[]) {
  	this.products = products;
  	this.total = total;
  	this.categories = categories;
  }

}
