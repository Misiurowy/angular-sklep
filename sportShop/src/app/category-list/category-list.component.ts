import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css'],
})
export class CategoryListComponent {
	categories: string[] = [];
	checked: string[] = [];
  constructor() { }
  filterName: string = "";
  catEmitted(categories: string[]): void {
  	this.categories = categories;
  }

  clicked(cat: string): void{
  	if (this.checked.indexOf(cat)==-1) {
  		this.checked.push(cat);
  	}else{
  		let index= this.checked.indexOf(cat);
  		this.checked.splice(index,1);
  	}
  	this.checked = this.getCopy(this.checked);
  }
  getCopy(cat: string[]): string[]{
  	let newcat:string[] = [];
  	for(let c of cat){
  		newcat.push(c);
  	}
  	return newcat;
  }

  isActive(cat: string): string{
  	if(this.checked.indexOf(cat)!=-1){
  		return "checked";
  	}else{
  		return "unchecked";
  	}
  }

}
