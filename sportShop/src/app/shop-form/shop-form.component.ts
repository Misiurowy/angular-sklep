import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TrolleyService } from '../trolley.service'
import { Product } from '../product/product.component'
import { Http} from '@angular/http'
import { Observable } from 'rxjs/Rx';
@Component({
  selector: 'app-shop-form',
  templateUrl: './shop-form.component.html',
  styleUrls: ['./shop-form.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ShopFormComponent implements OnInit {
	price: number = 0;
  name: string="";
  order: string;
  afterSend: boolean = false;
  error = false;
  url: string = "http://localhost:5000/orders"
  constructor(private http:Http,private trolleyService: TrolleyService) { }

  ngOnInit() {
  	let tmp: Product[] = this.trolleyService.getBoughtList();
    this.order = JSON.stringify(tmp);
  	for(let p of tmp){
  		this.price+=p.price;
  	}
  }

  onSubmit(form: any): void{
      console.log(form);
      console.log(form.name.length);
      if(form.name.length<3 || form.lastname.length<3 || form.address.length<5){
        this.error =true;
        return;
      }
      form.finished=false;
      console.log(form);
      this.http.post(this.url,form).subscribe();
      this.afterSend = true;
      this.trolleyService.clear();
  }

}
