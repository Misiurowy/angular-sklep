import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JsonpModule, HttpModule } from '@angular/http'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { Product } from './product/product.component';
import { ProductPaginationComponent } from './product-pagination/product-pagination.component';
import { CategoryuComponent } from './categoryu/categoryu.component';
import { ProductsService} from './products-service.service';
import { PromotionSocketService } from './promotion-socket.service'
import { ProductListData } from './product-list-data/product-list-data.component';
import { AppRoutingModule } from './/app-routing.module';
import { TrolleyComponent } from './trolley/trolley.component';
import { ShopMainComponent } from './shop-main/shop-main.component';
import { ShopFormComponent } from './shop-form/shop-form.component'
import { TrolleyService } from './trolley.service';
import { AdminPageComponent } from './admin-page/admin-page.component'

@NgModule({
  declarations: [
    AppComponent,
    CategoryListComponent,
    ProductListComponent,
    Product,
    ProductPaginationComponent,
    CategoryuComponent,
    ProductListData,
    TrolleyComponent,
    ShopMainComponent,
    ShopFormComponent,
    AdminPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    JsonpModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ProductsService, TrolleyService, PromotionSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
