import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Http, Jsonp, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { Product } from '../product/product.component'
import 'rxjs/add/operator/map'

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AdminPageComponent implements OnInit {
	private securityId: string;
	private sessionId: string = "";
	private url = "http://localhost:5000";
	private  username: string ="";
	private password: string ="";
	private logged: boolean =false;
	private loginError: boolean=false;
	private orders: any[] = [];
	private products: any;
	private showOrders = false;
	private showProducts = false;
	private showOrdersDetailForm = false;
	private showAddPromotion = false;
	private order;
	private productList: Product[];
	private promotion: {_id: string,percentage:number,duration:number}= {_id: "",percentage: 0,duration: 0};
	constructor(private http:HttpClient, private jsonp:Jsonp) { }

	ngOnInit() {

	}

	switchShowAddPromotion(){
		this.showAddPromotion = true;
		this.showOrdersDetailForm = false;
		this.showOrders= false;
		this.showProducts = false;
	}

	transferToFinished(order: any){
		order.finished = true;
		console.log(this.order);
		this.http.put(this.url+"/orders/"+order._id,order, {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=>{
				this.listOrders();
			});
	}

	sendPromotion(){
		this.http.post(this.url+'/putPromotion',this.promotion, {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=>{
				console.log(res);
			}
		);
		this.showAddPromotion = false;
	}

	checkUser(){
		this.http.get(this.url+"/users/me").subscribe(
			res=>{
				console.log(res);
			}
		);
	}

	authenticate(form: any){
		console.log(form);
		this.username = form.username;
		this.password = form.password;
		this.http.post(this.url+"/users/login",form).subscribe(
			res => {
				console.log(res);
				this.securityId = (<{securityid: string}>res).securityid;
				console.log(this.securityId);
				this.logged=true;
				this.loginError=false;
			},
			error =>{
				console.log(error);
				this.logged=false;
				this.loginError=true;
			} 
		);
	}

	logout(){
		this.http.post(this.url+"/users/logout","", {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
						error=>{
				console.log("unsuccessfull logout");
			},
			res=>{
				this.securityId = "";
				this.logged=false;
				this.showOrders=false;
				this.showProducts=false;
				this.showOrdersDetailForm=false;
				console.log("logout succesful");
			}

		);
		
	}

	addProduct(form: any){
		console.log(form);
		this.http.post(this.url+"/products",form, {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=>{
				console.log(res);
				this.showProducts = false;
			});
	}

	listProducts(){
		this.showAddPromotion = false;
		this.http.get(this.url+"/products").subscribe(
			res=> {
				this.products = res;
				console.log(res);
				this.showProducts = true;
				this.showOrders=false;
				this.showOrdersDetailForm=false;
			},
			error=>{
				console.log("cannot download orders");
			}
			)
	}

	addNewOrder(){
		this.order.order = JSON.stringify(this.productList);
		delete this.order._id;
		this.http.post(this.url+"/orders",this.order, {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=>{
				this.listOrders();
			});
	}

	listOrders(){
		this.showAddPromotion = false;
		this.showProducts = false;
		this.http.get(this.url+"/orders", {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=> {
				this.orders = [];
				let tmp = <{finished: String}[]>res;
				for(let ord of tmp){
					if(!ord.finished){
						this.orders.push(ord);
					}
				}
				console.log(tmp);

				this.showOrders=true;
				this.showOrdersDetailForm=false;
			},
			error=>{
				console.log("cannot download orders");
			}
			)
	}

		listRealizedOrders(){
		this.showAddPromotion = false;
		this.showProducts = false;
		this.http.get(this.url+"/orders", {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=> {
				this.orders = [];
				let tmp = <{finished: String}[]>res;
				for(let ord of tmp){
					if(ord.finished){
						this.orders.push(ord);
					}
				}
				
				console.log(tmp);

				this.showOrders=true;
				this.showOrdersDetailForm=false;
			},
			error=>{
				console.log("cannot download orders");
			}
			)
	}

	showOrderDetails(order){
		this.order = Object.assign({},order);
		this.productList = JSON.parse(order.order);
		this.showOrdersDetailForm=true;
		console.log(this.order);
	}

	updateOrder(){
		this.order.order = JSON.stringify(this.productList);
		console.log(this.order);
		this.http.put(this.url+"/orders/"+this.order._id,this.order, {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=>{
				this.listOrders();
			});
	}

	deleteOrder(){
		this.http.delete(this.url+"/orders/"+this.order._id, {headers: new HttpHeaders().set('Authorization', this.securityId)}).subscribe(
			res=>{
				this.listOrders();
			},
			error=>{
				console.log(error);
			}
			)
	}
	deleteProductFromOrder(product:Product){
		let index = this.productList.indexOf(product);
		if(this.productList[index].amount>1){
			this.productList[index].amount--;
		}else{
			this.productList.splice(index,1);
		}
	}
}
interface UserResponse {

}
