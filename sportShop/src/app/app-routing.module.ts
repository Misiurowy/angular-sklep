import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TrolleyComponent } from './trolley/trolley.component'
import { CategoryListComponent } from './category-list/category-list.component'
import { ShopMainComponent } from './shop-main/shop-main.component'
import { ShopFormComponent } from './shop-form/shop-form.component'
import { AdminPageComponent } from './admin-page/admin-page.component'

const routes: Routes = [
	{path: 'trolley', component: TrolleyComponent},
	{path: 'home', component: ShopMainComponent},
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: 'form', component: ShopFormComponent},
	{ path: 'admin', component: AdminPageComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
