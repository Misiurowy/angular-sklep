import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryuComponent } from './categoryu.component';

describe('CategoryuComponent', () => {
  let component: CategoryuComponent;
  let fixture: ComponentFixture<CategoryuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
