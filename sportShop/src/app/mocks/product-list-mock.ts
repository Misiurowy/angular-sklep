import  { Product } from '../product/product.component'

export const PRODUCTS: Product[] = [
	{name: "Football", description: "A ball? sport? who knows!",price: 39, category: "Football", amount: 4},
	{name: "Training shoes", description: "Shoes for horse galloping. Really, the name is kinda selfexplanatory",price: 99, category: "Football", amount: 4},
	{name: "Good shirt", description: "Shirt. Good. Really.",price: 29, category: "Football", amount: 4},
	{name: "Skies, 2 pack", description: "Get 2, pay like for 1!",price: 39, category: "Winter sports", amount: 4},
	{name: "Snowboard", description: "Don't get your hopes up, you don't get 2 here.",price: 39, category: "Winter sports", amount: 4},
	{name: "Skiing poles", description: "Just simple poles. But they're used in skiing so they got fancy name.",price: 39, category: "Winter sports", amount: 4},
	{name: "Spaghetti", description: "You know it's a sport shop, right?",price: 39, category: "Pasta", amount: 4},
]
