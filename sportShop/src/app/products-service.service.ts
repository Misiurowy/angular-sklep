import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Product } from './product/product.component'
import { PRODUCTS } from './mocks/product-list-mock'
import { ProductListData } from './product-list-data/product-list-data.component'
import { Http } from '@angular/http'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
@Injectable()
export class ProductsService {

  jsonAddress: string = "http://localhost:5000/products"

  constructor(private jsonp: Http) { }
  
  downloadProducts (): Observable<Product[]>{
    let ob:Observable<Product[]>  = this.jsonp.get(this.jsonAddress).map((res) => res.json());//.catch((err) =>{ return Observable.empty<Product[]>()});
    return ob;
  }
}
