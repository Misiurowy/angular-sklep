import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';

@Injectable()
export class PromotionSocketService {
	private url = 'http://localhost:5000';
	private socket;

 	constructor() { }

    getMessages() {
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('promotion', (data) => {
        observer.next(data);    
      });
      return () => {
        this.socket.disconnect();
      };  
    })     
    return observable;
  } 

}
