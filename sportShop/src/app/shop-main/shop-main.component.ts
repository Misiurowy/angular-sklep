import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TrolleyService } from '../trolley.service'
import { Product } from '../product/product.component'
@Component({
  selector: 'app-shop-main',
  templateUrl: './shop-main.component.html',
  styleUrls: ['./shop-main.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ShopMainComponent implements OnInit {

  boughtItems:number = 0;
	boughtPrice:number = 0;
  constructor(private trolleyService: TrolleyService) {
  	let tmp: Product[] = this.trolleyService.getBoughtList();
  	for(let prod of tmp){
  		this.boughtPrice += prod.price*prod.amount;
      this.boughtItems += prod.amount;
  	}
  	trolleyService.productBought$.subscribe(
  			product => {
  				this.boughtItems+= product.amount;
  				this.boughtPrice+=product.price*product.amount;
  			}
  		)
  }


  ngOnInit() {
  }

}
