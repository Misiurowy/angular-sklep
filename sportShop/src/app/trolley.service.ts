import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { Product } from './product/product.component'
@Injectable()
export class TrolleyService {

	private productList:Product[] = [];
	private productBoughtSource = new Subject<Product>();

	productBought$ = this.productBoughtSource.asObservable();

	sendBoughtProduct(product: Product){
		this.productBoughtSource.next(product);
		for(let prod of this.productList){
			if(prod.name == product.name && prod.description == product.description){
				prod.amount += product.amount;
				console.log(this.productList);
				console.log(product);
				return;
			}
		}
		this.productList.push(product);
	}

	getBoughtList():Product[]{
		return this.productList;
	}

  constructor() { }
  delete(index: number){
  	if(this.productList[index].amount>1){
  		this.productList[index].amount--;
  	}else{
  		this.productList.splice(index,1);
  	}
  }

  clear(){
    this.productList = [];
  }

}

