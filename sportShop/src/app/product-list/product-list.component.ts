import { Component,ViewEncapsulation, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { Product } from '../product/product.component';
import { ProductsService } from '../products-service.service'
import { TrolleyService } from '../trolley.service'
import { PromotionSocketService } from '../promotion-socket.service'
@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductListComponent implements OnInit, OnChanges {
	@Input() onlyCat: string[];
	 ngOnChanges(changes: SimpleChanges) {
     console.log(this.filterName);
   	this.page = 1;
   	this.updateProducts();
  }
	@Output() emitCat = new EventEmitter<string[]>();
  promotion: {duration: number, percentage: number, _id: string}[] = []
  allproducts: Product[]=[];
	products: Product[];
  @Input() filterName: string = "";
  priceBottom: number = 0;
  priceTop: number = 0;
  amounts: number[];
  isError: boolean = false;
  loading = false;
  total = 0;
  page = 1;
  limit = 3;
  connection;
  constructor(private promotionService : PromotionSocketService, private productsService : ProductsService, private trolleyService: TrolleyService) { }

  isOnPromotion(product: any):boolean{
    for(let prom of this.promotion){
      if(product._id==prom._id){
         return true;
      }
    }
    return false;
  }

  ngOnInit(){
  	this.getProducts();
    this.connection = this.promotionService.getMessages().subscribe(message => {
      let tmp: {duration: number, percentage: number, _id: string} = <{duration: number, percentage: number, _id: string}>message;
      if(tmp.duration>0){
        this.promotion.push(tmp);
      }else{
        for(let prom of this.promotion){
          if(prom._id==tmp._id){
            this.promotion.splice(this.promotion.indexOf(prom),1);
          }
        }
      }
      
      console.log(message);
    })

  } 

  updateProducts():void{
    let tmp: Product[] = this.filter();
    tmp = this.filterByName(tmp);
    this.products = tmp.slice((this.page-1)*this.limit,(this.page-1)*this.limit+this.limit)
    this.amounts = [];
    for(let prod of this.products){
      this.amounts.push(1);
    }
  }

  promotionPercentage(product: any): number{
    for(let prom of this.promotion){
      if(product._id==prom._id){
         return prom.percentage;
      }
    }
    return 0;
  }

  regulateWitTrolley(){
    let list: Product[] = this.trolleyService.getBoughtList();
    for(let bought of list){
      for(let prod of this.allproducts){
        if(bought.name == prod.name && bought.description == prod.description){
          prod.amount-=bought.amount;
          continue;
        }
      }
    } 
  }

  increase(index:number){
    this.amounts[index]+=1;
  }

  decrease(index:number){
    this.amounts[index]-=1;
  }

  getProducts():void {
  	this.loading = true;
    this.productsService.downloadProducts().subscribe( 
      data => {
        this.allproducts = data;
        this.regulateWitTrolley();
        this.updateProducts();
        this.emitCat.emit(this.getCategories(this.allproducts));
        this.loading = false;
      },
      error=> {
        this.isError = true;
        console.log("trololo");
        console.log(error);


      }
    )
  }

   goToPage(n: number): void {
    this.page = n;
    this.updateProducts();
  }

  onNext(): void {
    this.page++;
    this.updateProducts();
  }

  onPrev(): void {
    this.page--;
    this.updateProducts();
  }

  toTrolley(prod: Product,index: number){
    let tmp = Object.assign({}, prod);
    tmp.amount=this.amounts[index];
    prod.amount-=this.amounts[index];
    this.amounts[index]=1;
    this.trolleyService.sendBoughtProduct(tmp);
  }


  private getCategories(products: Product[]): string[]{
    let categories : string[] = [];
    for(let prod of products){
      if( categories.indexOf(prod.category)=== -1){
        categories.push(prod.category);
      }
    }
    return categories;
  }

  private filterByName(list: Product[]):Product[]{
    if(this.filterName.length<1){
      return list;
    }
    let newList:Product[] = [];
    for(let prod of list){
      if(prod.name.indexOf(this.filterName)!=-1){
        newList.push(prod);
      }
    }
    this.total = newList.length;
    return newList;
  }


  private filter():Product[]{
    if(this.onlyCat.length==0){
      this.total = this.allproducts.length;
      return this.allproducts;
    }
    let list:Product[] = [];
    for(let prod of this.allproducts){
      if(this.onlyCat.indexOf(prod.category)!=-1){
        list.push(prod);
      }
    }
    this.total = list.length;
    return list;
  }
}
