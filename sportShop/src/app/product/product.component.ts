import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class Product{
	name: string;
	description: string;
	price: number;
	category: string;
  amount: number;
//  constructor() { }
  constructor(name?: string, description?: string, price?: number, category?: string, amount?: number){
  	this.name = name;
  	this.description = description;
  	this.price = price;
  	this.category= category;
    this.amount = amount;
  }
}
