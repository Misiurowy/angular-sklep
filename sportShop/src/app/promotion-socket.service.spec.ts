import { TestBed, inject } from '@angular/core/testing';

import { PromotionSocketService } from './promotion-socket.service';

describe('PromotionSocketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PromotionSocketService]
    });
  });

  it('should be created', inject([PromotionSocketService], (service: PromotionSocketService) => {
    expect(service).toBeTruthy();
  }));
});
