import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TrolleyService } from '../trolley.service'
import { Product } from '../product/product.component'

@Component({
  selector: 'app-trolley',
  templateUrl: './trolley.component.html',
  styleUrls: ['./trolley.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TrolleyComponent implements OnInit {
	boughtItems: Product[] = [];
  constructor(private trolleyService: TrolleyService) { }

  ngOnInit() {
  	this.boughtItems = this.trolleyService.getBoughtList();
  }

  delete(i: number){
  	this.trolleyService.delete(i);
  	console.log(i);
  }


}